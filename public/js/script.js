
(function ($, window, document, undefined) {
    'use strict';

	function initReady() {
        
	}

	function initLoad() {
        $('#down').on('click', function() {
            $('html, body').animate({
                scrollTop: $("#slider_promocional").offset().top
            }, 600);
        });

        $(".hover_underline").hover(function(){
                $('.item_titulo').css("text-decoration", "underline");
            }, function(){
                $('.item_titulo').css("text-decoration", "none");
        });

        $('#downarrow').on('click', function() {
            $('html, body').animate({
                scrollTop: $("#slider_promocional").offset().top
            }, 600);
        });

        $(".open-nav").on("click", function(){
            $(this).toggleClass("open");
            TweenMax.to('#main-menu', .50, { css: { width: '0%' }, delay: .2, ease: Expo.linear });
            if ( $(this).hasClass("open") ) {
                TweenMax.to('#main-menu', .50, { css: { width: '100%' }, delay: .2, ease: Expo.linear });
            } 
        });

        $(".open-nav-responsive").on("click", function(){
            $(this).toggleClass("open-responsive");
        });

		$('#slider_promocional .owl-carousel').owlCarousel({
            loop: true,
            nav: true,
            dots: true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });

        $('#productos_home .owl-carousel').owlCarousel({
            loop: true,
            nav: true,
            dots: true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });

        $('#hoy_quiero .owl-carousel').owlCarousel({
            loop: true,
            nav: true,
            autoplay: true,
            dots: true,
            margin: 15,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3
                },
                1000:{
                    items:4
                }
            }
        });

        $('.lonuevo_contenido .owl-carousel').owlCarousel({
            loop: true,
            nav: false,
            autoplay: false,
            dots: true,
            margin: 0,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });
        $('.interesar .owl-carousel').owlCarousel({
            loop: true,
            nav: true,
            autoplay: false,
            dots: false,
            margin: 0,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:2
                }
            }
        });
        $('.recomendados_productos-carousel').owlCarousel({
            center: true,
            items:1,
            loop:true,
            margin:0,
            nav: false,
            doots: false,
            responsive: {
                0: {
                    doots: false
                },
                600:{
                    items: 4,
                    doots: false
                }
            }
        });
        $('.mejores_momentos .owl-carousel').owlCarousel({
            loop: true,
            nav: false,
            autoplay: false,
            dots: true,
            margin: 0,
            responsive:{
                0:{
                    items:2
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });

        $('.radio').on('click', function(){
            $(this).find('.label').css('background-color','#fff')
        });
	}
    

    $(document).on('ready', initReady);
    $(window).on('load', initLoad);
})(jQuery, window, document);