
(function ($, window, document, undefined) {
    'use strict';

	function initReady() {
        
	}

	function initLoad() {
        $('#down').on('click', function() {
            $('html, body').animate({
                scrollTop: $("#slider_promocional").offset().top
            }, 600);
        });

        $(".hover_underline").hover(function(){
                $('.item_titulo').css("text-decoration", "underline");
            }, function(){
                $('.item_titulo').css("text-decoration", "none");
        });

        $('#downarrow').on('click', function() {
            $('html, body').animate({
                scrollTop: $("#slider_promocional").offset().top
            }, 600);
        });

        $(".open-nav").on("click", function(){
            $(this).toggleClass("open");
            TweenMax.to('.nav_second', .50, { css: { right: '-100%' }, delay: .2, ease: Expo.linear });
            TweenMax.to('.nav_ppal', .50, { css: { top: '-100%', height: '0vh' }, autoAlpha:1, delay: .2, ease: Power2.easeOut });
            if ( $(this).hasClass("open") ) {
                TweenMax.to('.nav_second', .50, { css: { right: '0%' }, delay: .2, ease: Expo.linear });
                TweenMax.to('.nav_ppal', .50, { css: { top: '0%', height: '70vh' }, autoAlpha:1, delay: .2, ease: Power2.easeOut });
            } 
        });

        $(".volver_menu").on("click", function(){
            $('html').find('.open-nav').removeClass('open');
            var nav = $('html').find('.nav_ppal');
            var second = $('html').find('.nav_second');
            TweenMax.to(second, .50, { css: { right: '-100%' }, delay: .2, ease: Expo.linear });
            TweenMax.to(nav, .50, { css: { top: '-100%', height: '0vh' }, autoAlpha:1, delay: .2, ease: Power2.easeOut });
        });

        $(".open-nav-responsive").on("click", function(){
            $(this).toggleClass("open-responsive");
            TweenMax.to('.nav_second', .50, { css: { right: '-100%' }, delay: .2, ease: Expo.linear });
            TweenMax.to('.nav_ppal', .50, { css: { top: '-115%', height: '0vh' }, autoAlpha:1, delay: .2, ease: Power2.easeOut });
            if ( $(this).hasClass("open-responsive") ) {
                TweenMax.to('.nav_second', .50, { css: { right: '0%' }, delay: .2, ease: Expo.linear });
                TweenMax.to('.nav_ppal', .50, { css: { top: '9%', height: '70vh' }, autoAlpha:1, delay: .2, ease: Power2.easeOut });
            }
        });

		$('#slider_promocional .owl-carousel').owlCarousel({
            loop: true,
            nav: true,
            dots: true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });

        $('#productos_home .owl-carousel').owlCarousel({
            loop: true,
            nav: true,
            dots: true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });

        $('#hoy_quiero .owl-carousel').owlCarousel({
            loop: true,
            nav: true,
            autoplay: true,
            dots: true,
            margin: 15,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3
                },
                1000:{
                    items:4
                }
            }
        });

        $('.lonuevo_contenido .owl-carousel').owlCarousel({
            loop: true,
            nav: false,
            autoplay: false,
            dots: true,
            margin: 0,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });
        $('.interesar .owl-carousel').owlCarousel({
            loop: true,
            nav: true,
            autoplay: false,
            dots: false,
            margin: 0,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:2
                }
            }
        });
        $('.recomendados_productos-carousel').owlCarousel({
            center: true,
            items:1,
            loop:true,
            margin:0,
            nav: false,
            doots: false,
            responsive: {
                0: {
                    doots: false
                },
                600:{
                    items: 4,
                    doots: false
                }
            }
        });
        $('.mejores_momentos .owl-carousel').owlCarousel({
            loop: true,
            nav: false,
            autoplay: false,
            dots: true,
            margin: 0,
            responsive:{
                0:{
                    items:2
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });

        $('.radio').on('click', function(){
            $(this).find('.label').css('background-color','#fff')
        });

        $("#edit-search-block-form--2").attr( "required", "required" );

        // height depend whit
        //Categorías
        var same = $('.p_samesize').width();
        $('.p_samesize').css({
            'height': same + 'px'
        });

        var width = $(window).width();
        $(window).on('resize', function(){
           if($(this).width() != width){
               var same = $('.p_samesize').width();
                $('.p_samesize').css({
                    'height': same + 'px'
                });
            }
        });


        //Menú

        var width = $(window).width();
        $(window).on('resize', function(){
           if($(this).width() != width){
               var same = $('.categoria').width();
                $('.categoria').css({
                    'height': same + 'px'
                });
            }
        });
        
        var same = $('.categoria').width();
        $('.categoria').css({
            'height': same + 'px'
        });

        //Lo nuevo
        var width = $(window).width();
        $(window).on('resize', function(){
           if($(this).width() != width){
               var same = $('.recomendados_producto').width();
                $('.recomendados_producto').css({
                    'height': same + 'px'
                });
            }
        });

        var same = $('.recomendados_producto').width();
        $('.recomendados_producto').css({
            'height': same + 'px'
        });

        var width = $(window).width();
        $(window).on('resize', function(){
           if($(this).width() != width){
               var same = $('.img_circle ').width();
                $('.img_circle ').css({
                    'height': same + 'px'
                });
            }
        });

        var same = $('.img_circle ').width();
        $('.img_circle ').css({
            'height': same + 'px'
        });

        // End depend
	}

    $(document).on('ready', initReady);
    $(window).on('load', initLoad);
})(jQuery, window, document);

